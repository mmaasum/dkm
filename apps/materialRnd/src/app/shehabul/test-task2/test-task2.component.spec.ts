import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTask2Component } from './test-task2.component';

describe('TestTask2Component', () => {
  let component: TestTask2Component;
  let fixture: ComponentFixture<TestTask2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestTask2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTask2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
