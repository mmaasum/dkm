import { BrowserModule } from '@angular/platform-browser';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatMenuModule} from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { LocationPickerComponent } from './location-picker/location-picker.component';
import { MapComponent } from './map/map.component';
import { TopNavComponent } from './shared/components/top-nav/top-nav.component';
import { SideNavComponent } from './shared/components/side-nav/side-nav.component';




// const routes: Routes = [
  
//   { path: 'no-data', component: HomeComponent },
//   { path: 'data', component: DataComponent }
// ];

@NgModule({
  declarations: [
    AppComponent,
    LocationPickerComponent,
    MapComponent,
    TopNavComponent,
    SideNavComponent
  ],
  imports: [
    MatSliderModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatMenuModule, 
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatCardModule,
    MatInputModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,

    // RouterModule.forRoot(
    //   routes,
    //   { enableTracing: true } 
    // )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
