import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import * as L from 'leaflet';
import { Location } from '../shared/models/location'

// export interface Task {
//   name: string;
//   details: string;
//   location: string;
// }
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.css']
})
export class LocationPickerComponent {

  latitude
  longitude 
  mLat:string;
  mLng:string;
  coordinate:string;
  tobeDelete:string;
  tasks: Location[] = [];
  private map;

  constructor() {}

  geoTaskFormControl = new FormControl('', [
    Validators.required,
  ]);
  geoLocationFormControl = new FormControl('', [
    Validators.required,
  ]);

  matcher = new MyErrorStateMatcher();

  tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18, attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

  icon = {
    icon: L.icon({
      iconSize: [ 25, 41 ],
      iconAnchor: [ 13, 0 ],
      iconUrl: '../assets/marker-icon.png',
      shadowUrl: '../assets/marker-shadow.png'
    })
  };

  getLatLng(){
    this.map.on('click', (e) => {
      this.mLat = e.latlng.lat;
      this.mLng = e.latlng.lng;
      this.coordinate=this.mLat +','+this.mLng;
     
    });
  }

  delTask(name){
    this.tasks = this.tasks.filter(item => item != name);
    const index: number = this.tasks.indexOf(name);
    console.log(index);
    if (index !== -1) {
      console.log(this.tasks);
        this.tasks.splice(index, 1);
        console.log(this.tasks);
    } 
  }

  addTask(name, details, location){
    if(this.geoLocationFormControl.valid && this.geoTaskFormControl.valid){
      this.tasks.push({name: name.value, details: details.value, location: location.value});
      name.value = '';
      details.value = '';
      location.value = '';
    }
  }

  goLocation(coordinate, task){
    this.latitude=coordinate.split(',')[0];
    this.longitude=coordinate.split(',')[1];

    this.tobeDelete=task;
    console.log(this.tobeDelete);
    const marker = L.marker([this.latitude, this.longitude], this.icon).addTo(this.map);
    marker.bindPopup('<b>Welcome to!</b><br>'+task).openPopup();
  }

  ngAfterViewInit(): void {
    this.initMap();
    this.tiles.addTo(this.map);
    const marker = L.marker([23.777176, 90.3], this.icon).addTo(this.map);
    marker.bindPopup('<b>Hello world!</b><br>I am a popup.').openPopup();

    this.getLatLng();
  }

  private initMap(): void {
    this.map = L.map('map', {
      center: [ 23.777176, 90.399452 ],
      zoom: 7
    });
  }
}
